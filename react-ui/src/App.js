import React, { Component } from 'react';
import './App.css';

const posts = [{
  id: 1,
  likes: []
}, {
  id: 2,
  likes: ['Peter']
}, {
  id: 3,
  likes: ['John', 'Mark']
}, {
  id: 4,
  likes: ['Paul', 'Lilly', 'Alex']
}, {
  id: 5,
  likes: ['Sarah', 'Michelle', 'Alex', 'John']
}];
const postLikesOutput = [];

class App extends Component {
  

  render() {
        
    postLikes();
    function postLikes() {
      posts.forEach(function(element) {
        if (element.likes.length > 3) {
          postLikesOutput.push({
            "id": element.id,
            "text": element.likes[0] + ", " + element.likes[1] + " and " + (element.likes.length - 2) + " others like this",
          })
        } else if (element.likes.length > 2) {
          postLikesOutput.push({
            "id": element.id,
            "text": element.likes[0] + ", " + element.likes[1] + " and " + element.likes[2] + " like this",
          })
        } else if (element.likes.length > 1) {
          postLikesOutput.push({
            "id": element.id,
            "text": element.likes[0] + " and " + element.likes[1] + " like this",
          })
        } else if (element.likes.length > 0) {
          postLikesOutput.push({
            "id": element.id,
            "text": element.likes[0] + " likes this",
          })
        } else {
          postLikesOutput.push({
            "id": element.id,
            "text": "No one likes this",
          })
        }
      });
    }
    
    return (      
      <div className="App">
        <div className="App-header">
          <img src="https://prodigygame.com/assets/images/svg/ed/ed-balloon.svg" className="App-logo" alt="logo" />
          <h2>Prodigy - Goran Vukancic</h2>
        </div>
        <table className="likes">
            {
              postLikesOutput.map(function(ub) {
                    return (
                      <tbody key={ub.id}>
                        <tr>
                            <td>id: {ub.id} - </td>
                            <td className="justify"> {ub.text}</td>
                        </tr>
                        </tbody>
                    )
                })
            }
        </table>
        <img src="https://goo.gl/6M8Att" alt=":(" width="90%" height="90%"></img>
      </div>
    );
  }
}

export default App;